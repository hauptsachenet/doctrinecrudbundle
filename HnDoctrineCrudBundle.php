<?php

namespace Hn\DoctrineCrudBundle;

use Hn\DoctrineCrudBundle\DependencyInjection\Compiler\RegisterCrudControllersCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class HnDoctrineCrudBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new RegisterCrudControllersCompilerPass());
    }
}


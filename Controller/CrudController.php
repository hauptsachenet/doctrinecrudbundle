<?php

namespace Hn\DoctrineCrudBundle\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityNotFoundException;
use Hn\DoctrineCrudBundle\Model\CrudDataManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Templating\Helper\StopwatchHelper;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Stopwatch\Stopwatch;

class CrudController extends Controller
{
    /** @var string */
    private $crudName;

    /** @var CrudDataManagerInterface */
    private $dataManager;

    /** @var array */
    private $templatePaths;

    /** @var string */
    private $newFormType;

    /** @var string */
    private $editFormType;

    /**
     * @return Stopwatch|StopwatchHelper
     */
    protected function getStopwatch()
    {
        return $this->get('templating.helper.stopwatch');
    }

    protected function start($section)
    {
        $this->getStopwatch()->start($section, "crud");
    }

    protected function stop($section)
    {
        $this->getStopwatch()->stop($section, "crud");
    }

    /**
     * @param string $crudName
     */
    public function setCrudName($crudName)
    {
        $this->crudName = $crudName;
    }

    /**
     * @return string
     */
    public function getCrudName()
    {
        return $this->crudName;
    }

    /**
     * @param \Hn\DoctrineCrudBundle\Model\CrudDataManagerInterface $dataManager
     */
    public function setDataManager($dataManager)
    {
        $this->dataManager = $dataManager;
    }

    /**
     * @param Request $request
     */
    protected function prepareDataManager(Request $request)
    {
        // can be overwritten to give url params to data manager
    }

    /**
     * @return \Hn\DoctrineCrudBundle\Model\CrudDataManagerInterface
     */
    public function getDataManager()
    {
        return $this->dataManager;
    }

    /**
     * @param array $templatePath
     */
    public function setTemplatePaths(array $templatePath)
    {
        $this->templatePaths = $templatePath;
    }

    /**
     * @return array
     */
    public function getTemplatePaths()
    {
        return $this->templatePaths;
    }

    /**
     * @param string $newFormType
     */
    public function setNewFormType($newFormType)
    {
        if (class_exists($newFormType)) {
            $this->newFormType = new $newFormType();
        } else {
            $this->newFormType = $newFormType;
        }
    }

    /**
     * @return string
     */
    public function getNewFormType()
    {
        return $this->newFormType;
    }

    /**
     * @param string $editFormType
     */
    public function setEditFormType($editFormType)
    {
        if (class_exists($editFormType)) {
            $this->editFormType = new $editFormType();
        } else {
            $this->editFormType = $editFormType;
        }
    }

    /**
     * @return string
     */
    public function getEditFormType()
    {
        return $this->editFormType;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    protected function createInstance(Request $request)
    {
        $dataInstance = $this->dataManager->createInstance();

        if (!$this->dataManager->isGranted('create', $dataInstance)) {
            throw new AccessDeniedHttpException();
        }

        return $dataInstance;
    }

    /**
     * @param string $action
     * @return string
     */
    protected function getTemplate($action)
    {
        $this->start("template lookup");

        $templating = $this->get('templating');
        foreach ($this->getTemplatePaths() as $templatePath) {
            $templatePath = $templatePath . $action . '.html.twig';

            if ($templating->exists($templatePath)) {
                $this->stop("template lookup");
                return $templatePath;
            }
        }

        throw new \RuntimeException("No template found for action '$action' in paths: " . implode(', ', $this->getTemplatePaths()));
    }

    /**
     * @param string $action
     * @param object $dataInstance
     * @param FormInterface $form
     * @return Response
     */
    protected function createEditResponse($action, $dataInstance, FormInterface $form)
    {
        $translator = $this->get('translator');

        if ($form->isSubmitted() && $form->isValid()) {

            $msg = $translator->trans($this->getCrudName() . " has been successfully saved");
            $this->get('session')->getFlashBag()->add('success', $msg);

            return $this->createSuccessResponse($action, $dataInstance, $form);
        } else {

            if ($form->isSubmitted() && !$form->isValid()) {

                $msg = $translator->trans($this->getCrudName() . " wasn't saved because of errors");
                $this->get('session')->getFlashBag()->add('error', $msg);
            }

            $this->start("create view");
            $formView = $form->createView();
            $this->stop("create view");

            return $this->createFormResponse($action, $dataInstance, $formView);
        }
    }

    /**
     * @param string $action
     * @param object $dataInstance
     * @param FormInterface $form
     * @return Response
     */
    protected function createSuccessResponse($action, $dataInstance, FormInterface $form)
    {
        $parameter = $this->editLinkParams($dataInstance, $this->get('request'));
        $url = $this->generateUrl('hn_doctrine_crud.' . $this->crudName . '.edit', $parameter);
        return $this->redirect($url);
    }

    /**
     * @param string $action
     * @param object $dataInstance
     * @param FormView $formView
     * @return Response
     */
    protected function createFormResponse($action, $dataInstance, $formView)
    {
        return $this->render($this->getTemplate($action), $this->formViewParams($dataInstance, $formView));
    }

    /**
     * @param Request $request
     * @param FormInterface $form
     * @param object $dataInstance
     */
    protected function submitForm(Request $request, FormInterface $form, $dataInstance)
    {
        $form->submit($request);

        if ($form->isValid()) {
            $this->dataManager->persist($dataInstance);
        }
    }

    /**
     * @param Request $request
     * @param FormInterface $form
     * @param object $dataInstance
     */
    protected function deleteEntity(Request $request, FormInterface $form, $dataInstance)
    {
        $form->submit($request);

        if ($form->isValid()) {
            $this->dataManager->remove($dataInstance);
        }
    }

    /**
     * @return ObjectManager
     */
    protected function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @param $dataInstance
     * @param $formView
     * @return array
     */
    protected function formViewParams($dataInstance, $formView)
    {
        return [
            'data' => $dataInstance,
            'form' => $formView,
            'crudName' => $this->getCrudName()
        ];
    }

    /**
     * @param mixed $dataInstance
     * @param Request $request
     * @return array
     */
    protected function createLinkParams($dataInstance, Request $request)
    {
        return array();
    }

    /**
     * @param mixed $dataInstance
     * @param Request $request
     * @return array
     */
    protected function editLinkParams($dataInstance, Request $request)
    {
        return array(
            'id' => $this->dataManager->getId($dataInstance)
        );
    }

    /**
     * @param mixed $id
     * @return mixed
     */
    protected function findInstance($id)
    {
        $this->start("find instance");
        try {
            $dataInstance = $this->dataManager->findInstance($id);
        } catch (EntityNotFoundException $e) {
            throw new NotFoundHttpException("could not load data", $e);
        }

        if (!$this->dataManager->isGranted('edit', $dataInstance)) {
            throw new AccessDeniedHttpException();
        }
        $this->stop("find instance");
        return $dataInstance;
    }

    public function newAction(Request $request)
    {
        $this->prepareDataManager($request);
        $dataInstance = $this->createInstance($request);
        $formOptions = $this->newFormOptions($dataInstance, $request);

        $this->start("create form");
        $form = $this->createForm($this->getNewFormType(), $dataInstance, $formOptions);
        $this->stop("create form");

        return $this->createEditResponse('new', $dataInstance, $form);
    }

    public function createAction(Request $request)
    {
        $this->prepareDataManager($request);
        $dataInstance = $this->createInstance($request);
        $formOptions = $this->newFormOptions($dataInstance, $request);

        $this->start("create form");
        $form = $this->createForm($this->getNewFormType(), $dataInstance, $formOptions);
        $this->stop("create form");

        $this->start("submit");
        $this->submitForm($request, $form, $dataInstance);
        $this->stop("submit");

        return $this->createEditResponse('new', $dataInstance, $form);
    }

    protected function newFormOptions($dataInstance, Request $request)
    {
        $parameter = $this->createLinkParams($dataInstance, $request);
        $action = $this->generateUrl('hn_doctrine_crud.' . $this->crudName . '.create', $parameter);

        return array(
            'action' => $action,
            'method' => 'POST'
        );
    }

    public function listAction(Request $request)
    {
        $this->prepareDataManager($request);
        $this->start("get list");
        $listData = $this->dataManager->getListData();
        $this->stop("get list");

        return $this->render($this->getTemplate('list'), array(
            'list' => $listData,
            'crudName' => $this->getCrudName()
        ));
    }

    public function editAction(Request $request, $id)
    {
        $this->prepareDataManager($request);
        $dataInstance = $this->findInstance($id);
        $formOptions = $this->editFormOptions($dataInstance, $request);

        $this->start("create form");
        $form = $this->createForm($this->getEditFormType(), $dataInstance, $formOptions);
        $this->stop("create form");

        return $this->createEditResponse('edit', $dataInstance, $form);
    }

    /**
     * @param Request $request
     * @param $id
     * @return array
     */

    public function updateAction(Request $request, $id)
    {
        $this->prepareDataManager($request);
        $dataInstance = $this->findInstance($id);
        $formOptions = $this->editFormOptions($dataInstance, $request);

        $this->start("create form");
        $form = $this->createForm($this->getEditFormType(), $dataInstance, $formOptions);
        $this->stop("create form");

        $this->submitForm($request, $form, $dataInstance);

        return $this->createEditResponse('edit', $dataInstance, $form);
    }

    protected function editFormOptions($dataInstance, Request $request)
    {
        $parameters = $this->editLinkParams($dataInstance, $request);
        $action = $this->generateUrl('hn_doctrine_crud.' . $this->crudName . '.update', $parameters);

        return array(
            'action' => $action,
            'method' => 'PUT'
        );
    }

    /**
     * @todo implement
     */
    public function deleteAction()
    {
//        $dataInstance = $this->getEntityProvider()->getEntity($id);
//        $form = $this->getFormProvider()->getDeleteForm($dataInstance);
//
//        $this->deleteEntity($request, $form, $dataInstance);
//
        return array();
    }
}

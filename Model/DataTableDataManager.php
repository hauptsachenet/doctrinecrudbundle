<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 07.10.14
 * Time: 15:44
 */

namespace Hn\DoctrineCrudBundle\Model;


use Hn\DataTablesBundle\Factory\DataTableFactory;
use Hn\DataTablesBundle\Model\DataTable;

class DataTableDataManager extends CrudEntityDataManager
{
    /**
     * @var string
     */
    protected $dataTableClass;

    /**
     * @var DataTable|null
     */
    protected $dataTableInstance;

    /**
     * @var DataTableFactory
     */
    protected $dataTableFactory;

    public function setDataTableClass($className)
    {
        $this->dataTableClass = $className;
    }

    protected function getDataTableInstance()
    {
        if ($this->dataTableInstance !== null) {
            return $this->dataTableInstance;
        }

        $dataTableInstance = new $this->dataTableClass();

        if (!$dataTableInstance instanceof DataTable) {
            $type = is_object($dataTableInstance) ? get_class($dataTableInstance) : gettype($dataTableInstance);
            throw new \RuntimeException("Expected DataTable instance, got $type");
        }

        return $this->dataTableInstance = $dataTableInstance;
    }

    public function setDataTableFactory(DataTableFactory $dataTableFactory)
    {
        $this->dataTableFactory = $dataTableFactory;
    }

    public function getListData()
    {
        return $this->dataTableFactory->createView($this->getDataTableInstance());
    }

    public function setOptions(array $options)
    {
        if (!array_key_exists('data_table', $options)) {
            throw new \LogicException("Missing configuration 'data_table'");
        }

        parent::setOptions($options);

        $this->setDataTableClass($options['data_table']);
    }
}
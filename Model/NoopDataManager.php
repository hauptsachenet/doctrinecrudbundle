<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 19.04.16
 * Time: 10:10
 */

namespace Hn\DoctrineCrudBundle\Model;


class NoopDataManager implements CrudDataManagerInterface
{
    /**
     * Creates an instance of the data that is managed.
     *
     * @return mixed
     */
    public function createInstance()
    {
        return array();
    }

    /**
     * Finds an instance of the managed data by id.
     * This id can be anything but should be scalar.
     *
     * findInstance(getId($instance)) === $instance
     *
     * @throws \Exception if the id can't be found
     * @param mixed $id
     * @return mixed
     */
    public function findInstance($id)
    {
        return null;
    }

    /**
     * This method returns a list of data for output reasons.
     * It does not have to be an array of data instances.
     * Just make sure the list template can handle it.
     *
     * @return mixed
     */
    public function getListData()
    {
        return [];
    }

    /**
     * Gets an identifier for the given instance.
     * Must throw an exception if an id can't be generated.
     *
     * @throws \Exception if the id can't be generated
     * @param mixed $traineeCrudObject
     * @return mixed
     */
    public function getId($traineeCrudObject)
    {
        return null;
    }

    /**
     * Saves changes to the instance provided
     *
     * @param mixed $traineeCrudObject
     * @return mixed
     */
    public function persist($traineeCrudObject)
    {
    }

    /**
     * Removes the instance provided
     *
     * @param mixed $instance
     * @return mixed
     */
    public function remove($instance)
    {
    }

    /**
     * Checks if the attributes are granted against the current authentication token and optionally supplied object.
     *
     * @param mixed $attributes
     * @param mixed $object
     *
     * @return bool
     */
    public function isGranted($attributes, $object = null)
    {
        return true;
    }
}
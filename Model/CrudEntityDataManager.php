<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 07.10.14
 * Time: 11:31
 */

namespace Hn\DoctrineCrudBundle\Model;


use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityNotFoundException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;

class CrudEntityDataManager implements CrudDataManagerInterface, ConfigurableDataManagerInterface
{
    /**
     * @var ObjectManager
     */
    protected $manager;

    /**
     * @var SecurityContextInterface
     */
    protected $context;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var string
     */
    protected $entityName;

    function __construct(ObjectManager $manager, SecurityContextInterface $context, LoggerInterface $logger)
    {
        $this->manager = $manager;
        $this->context = $context;
        $this->logger = $logger;
    }

    /**
     * @param string $className
     */
    public function setEntityName($className)
    {
        $this->entityName = $className;
    }

    /**
     * @param object $instance
     * @return \Doctrine\Common\Persistence\Mapping\ClassMetadata
     * @throws \RuntimeException
     */
    protected function getMetaData($instance)
    {
        if (!$this->manager->contains($instance)) {
            $type = is_object($instance) ? get_class($instance) : gettype($instance);
            throw new \RuntimeException("'$type' is not managed by the entity manager");
        }

        $metaData = $this->manager->getClassMetadata($this->entityName);

        $className = $metaData->getName();
        if (!$instance instanceof $className) {
            $type = is_object($instance) ? get_class($instance) : gettype($instance);
            throw new \RuntimeException("The data manager for '$this->entityName' can't handle '$type', expected '$className");
        }

        return $metaData;
    }

    /**
     * Creates an instance of the data that is managed.
     *
     * @return mixed
     */
    public function createInstance()
    {
        $meta = $this->manager->getClassMetadata($this->entityName);
        $className = $meta->getName();

        $entity = new $className();
        $this->logger->info("created new instance of '$this->entityName'", array($entity));
        return $entity;
    }

    /**
     * Finds an instance of the managed data by id.
     * This id can be anything but should be scalar.
     *
     * findInstance(getId($instance)) === $instance
     *
     * @throws \Exception if the id can't be found
     * @param mixed $id
     * @return mixed
     */
    public function findInstance($id)
    {
        $entity = $this->manager->find($this->entityName, $id);

        if (!is_object($entity)) {
            $this->logger->error("didn't find entity of type $this->entityName with id $id", array($entity));
            throw new EntityNotFoundException();
        }

        $this->logger->info("loaded entity of type $this->entityName with id $id", array($entity));

        return $entity;
    }

    /**
     * This method returns a list of data for output reasons.
     * It does not have to be an array of data instances.
     * Just make sure the list template can handle it.
     *
     * @return mixed
     */
    public function getListData()
    {
        return $this->manager->getRepository($this->entityName)->findAll();
    }

    /**
     * Gets an identifier for the given instance.
     * Must throw an exception if an id can't be generated.
     *
     * @throws \Exception if the id can't be generated
     * @param mixed $traineeCrudObject
     * @return mixed
     */
    public function getId($traineeCrudObject)
    {
        $metaData = $this->getMetaData($traineeCrudObject);
        $identifiers = $metaData->getIdentifierValues($traineeCrudObject);

        if (count($identifiers) !== 1) {
            throw new \RuntimeException("Cannot handle multiple identifiers");
        }

        return reset($identifiers);
    }

    /**
     * Saves changes to the instance provided
     *
     * @param mixed $traineeCrudObject
     * @return mixed
     */
    public function persist($traineeCrudObject)
    {
        $this->manager->persist($traineeCrudObject);
        $this->manager->flush();
    }

    /**
     * Removes the instance provided
     *
     * @param mixed $instance
     * @return mixed
     */
    public function remove($instance)
    {
        $this->manager->remove($instance);
        $this->manager->flush();
    }

    /**
     * Checks if the attributes are granted against the current authentication token and optionally supplied object.
     *
     * @param mixed $attributes
     * @param mixed $object
     *
     * @return bool
     */
    public function isGranted($attributes, $object = null)
    {
        return $this->context->isGranted($attributes, $object);
    }

    /**
     * Sets the given options form the configuration.
     *
     * @param array $options
     */
    public function setOptions(array $options)
    {
        if (!array_key_exists('entity_name', $options)) {
            throw new \LogicException("Missing configuration 'entity_name'");
        }

        $this->setEntityName($options['entity_name']);
    }
}
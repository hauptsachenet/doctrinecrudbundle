<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 03.12.14
 * Time: 12:16
 */

namespace Hn\DoctrineCrudBundle\Model;


interface ConfigurableDataManagerInterface extends CrudDataManagerInterface
{
    /**
     * Sets the given options form the configuration.
     *
     * @param array $options
     */
    public function setOptions(array $options);
}
<?php

namespace Hn\DoctrineCrudBundle\Tests\Configuration;

use Hn\DoctrineCrudBundle\DependencyInjection\Configuration;
use Symfony\Component\Config\Definition\Processor;

class ConfigurationTest extends \PHPUnit_Framework_TestCase
{

    public function configurationProvider()
    {
        $defaultProcessed = array(
            'data_manager' => 'testEntityProvider',
            'data_manager_options' => array(),
            'new_form_type' => 'form',
            'edit_form_type' => 'form',
            'controller' => array(
                'class' => 'Hn\DoctrineCrudBundle\Controller\CrudController',
                'templatePaths' => array('HnDoctrineCrudBundle:Crud:'),
                'actions' => array(
                    'new' => array('disabled' => false),
                    'list' => array('disabled' => false),
                    'edit' => array('disabled' => false),
                    'delete' => array('disabled' => false)
                )
            )
        );

        return array(

            array(
                array(
                    array('test' => array(
                        'data_manager' => 'testEntityProvider',
                        'new_form_type' => 'form',
                        'edit_form_type' => 'form',
                    ))
                ),
                array('test' => $defaultProcessed)
            ),

            array(
                array(
                    array('test' => array(
                        'data_manager' => 'testEntityProvider',
                        'new_form_type' => 'form',
                        'edit_form_type' => 'form',
                        'controller' => array(
                            'class' => 'Hn\DoctrineCrudBundle\Controller\CrudController'
                        )
                    ))
                ),
                array('test' => $defaultProcessed)
            ),
            array(
                array(
                    array('test' => array(
                        'data_manager' => 'testEntityProvider',
                        'new_form_type' => 'form',
                        'edit_form_type' => 'form',
                        'controller' => array(
                            'class' => 'Hn\DoctrineCrudBundle\Controller\CrudController'
                        )
                    ))
                ),
                array('test' => $defaultProcessed)
            ),
        );
    }

    /**
     * @dataProvider configurationProvider
     */
    public function testConfiguration($configs, $processedConfig)
    {
        $configuration = new Configuration();

        $processor = new Processor();
        $config = $processor->processConfiguration($configuration, $configs);

        $this->assertEquals($processedConfig, $config);
    }
}
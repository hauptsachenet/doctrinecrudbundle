<?php

namespace Hn\DoctrineCrudBundle\Tests\Configuration;

use Hn\DoctrineCrudBundle\DependencyInjection\Compiler\RegisterCrudControllersCompilerPass;
use Hn\DoctrineCrudBundle\DependencyInjection\HnDoctrineCrudExtension;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class HnDoctrineCrudExtensionTest extends KernelTestCase
{

    public function setUp()
    {
        self::bootKernel();
    }

    public function testBuild()
    {
        $this->assertTrue(self::$kernel->getContainer()->hasParameter('hn_doctrine_crud.config'));

        $containerBuilder = new ContainerBuilder();
        self::$kernel->getBundle('HnDoctrineCrudBundle')->build($containerBuilder);

        /** @var CompilerPassInterface[] $passes */
        $found = false;
        $passes = $containerBuilder->getCompilerPassConfig()->getBeforeOptimizationPasses();
        foreach ($passes as $pass) {
            if ($pass instanceof RegisterCrudControllersCompilerPass) {
                $found = true;
                break;
            }
        }

        $this->assertTrue($found, '"RegisterCrudControllersCompilerPass" compiler pass not found!');
    }

}
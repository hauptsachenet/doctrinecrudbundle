<?php

namespace Hn\DoctrineCrudBundle\Tests\Configuration;

use Hn\DoctrineCrudBundle\DependencyInjection\Compiler\RegisterCrudControllersCompilerPass;
use Hn\DoctrineCrudBundle\DependencyInjection\HnDoctrineCrudExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class RegisterCrudControllersCompilerPassTest extends \PHPUnit_Framework_TestCase
{

    public function testProcess()
    {
        $containerBuilder = new ContainerBuilder();

        $extension = new HnDoctrineCrudExtension();
        $extension->load(array(array('test' => array(
            'data_manager' => 'testEntityProvider',
            'new_form_type' => 'form',
            'edit_form_type' => 'form',
        ))), $containerBuilder);

        $pass = new RegisterCrudControllersCompilerPass();
        $pass->process($containerBuilder);

        $this->assertTrue($containerBuilder->hasDefinition('hn_doctrine_crud.test.controller'), 'ContainerBuilder has no "hn_doctrine_crud.user.controller" service definition');
    }
}
<?php

namespace Hn\DoctrineCrudBundle\DependencyInjection\Compiler;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class RegisterCrudControllersCompilerPass implements CompilerPassInterface
{
    /**
     * @var PropertyAccessor
     */
    private $pa;

    public function __construct()
    {
        $this->pa = PropertyAccess::createPropertyAccessor();
    }

    public function process(ContainerBuilder $container)
    {
        $config = $container->getParameter('hn_doctrine_crud.config');

        foreach ($config as $crudName => $crudConfig) {

            $this->generateControllerConfiguration($container, $crudName, $crudConfig);
            $this->generateDataManagerConfiguration($container, $crudName, $crudConfig);
        }
    }

    /**
     * @param ContainerBuilder $container
     * @param string $crudName
     * @param array $crudConfig
     */
    protected function generateControllerConfiguration(ContainerBuilder $container, $crudName, array $crudConfig)
    {
        $controllerClass = $this->pa->getValue($crudConfig, '[controller][class]');
        $definition = new Definition($controllerClass);

        $definition->addMethodCall('setContainer', array(new Reference('service_container')));
        $definition->addMethodCall('setCrudName', array($crudName));

        $dataManagerServiceId = $this->pa->getValue($crudConfig, '[data_manager]');
        $definition->addMethodCall('setDataManager', array(new Reference($dataManagerServiceId)));

        $formType = $this->pa->getValue($crudConfig, '[new_form_type]');
        $definition->addMethodCall('setNewFormType', array($formType));

        $formType = $this->pa->getValue($crudConfig, '[edit_form_type]');
        $definition->addMethodCall('setEditFormType', array($formType));

        $templatePaths = $this->pa->getValue($crudConfig, '[controller][templatePaths]');
        $definition->addMethodCall('setTemplatePaths', array($templatePaths));

        $controllerName = 'hn_doctrine_crud.' . $crudName . '.controller';
        $container->addDefinitions(array($controllerName => $definition));
    }

    protected function generateDataManagerConfiguration(ContainerBuilder $container, $crudName, array $crudConfig)
    {
        $options = $this->pa->getValue($crudConfig, '[data_manager_options]');

        // only do this if options are provided, else simply use the original
        if (empty($options)) {
            return;
        }

        $parent = $this->pa->getValue($crudConfig, '[data_manager]');
        $definition = new DefinitionDecorator($parent);

        $definition->addMethodCall('setOptions', array($options));

        $dataManagerName = 'hn_doctrine_crud.' . $crudName . '.data_manager';
        $container->addDefinitions(array($dataManagerName => $definition));

        $controllerName = 'hn_doctrine_crud.' . $crudName . '.controller';
        $controllerDefinition = $container->getDefinition($controllerName);
        $controllerDefinition->removeMethodCall('setDataManager');
        $controllerDefinition->addMethodCall('setDataManager', array(new Reference($dataManagerName)));
    }
}
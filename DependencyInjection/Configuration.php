<?php

namespace Hn\DoctrineCrudBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    /**
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('hn_doctrine_crud');
        $rootNode
            ->useAttributeAsKey('name')
            ->prototype('array')
            ->children()

            ->scalarNode('data_manager')->defaultValue('hn_doctrine_crud.entity_data_manager')->end()
            ->arrayNode('data_manager_options')->prototype('scalar')->end()->end()
            ->scalarNode('edit_form_type')->isRequired()->end()
            ->scalarNode('new_form_type')->isRequired()->end()
            ->arrayNode('controller')->addDefaultsIfNotSet()
            ->children()

            ->scalarNode('class')->defaultValue('Hn\DoctrineCrudBundle\Controller\CrudController')->end()
            ->arrayNode('templatePaths')->addDefaultChildrenIfNoneSet()->prototype('scalar')->defaultValue('HnDoctrineCrudBundle:Crud:')->end()->end()

            ->arrayNode('actions')->addDefaultsIfNotSet()->children()

            ->arrayNode('new')->addDefaultsIfNotSet()->children()
            ->booleanNode('disabled')->defaultFalse()->end()
            ->scalarNode('path')->defaultValue('/%s/create')->end()
            ->end()->end()

            ->arrayNode('list')->addDefaultsIfNotSet()->children()
            ->booleanNode('disabled')->defaultFalse()->end()
            ->scalarNode('path')->defaultValue('/%s/list')->end()
            ->end()->end()

            ->arrayNode('edit')->addDefaultsIfNotSet()->children()
            ->booleanNode('disabled')->defaultFalse()->end()
            ->scalarNode('path')->defaultValue('/%s/{id}/edit')->end()
            ->end()->end()

            ->arrayNode('delete')->addDefaultsIfNotSet()->children()
            ->booleanNode('disabled')->defaultFalse()->end()
            ->scalarNode('path')->defaultValue('/%s/{id}/delete')->end()
            ->end()->end()

            ->end()->end()

            ->end()->end()

            ->end()->end();

        return $treeBuilder;
    }
}

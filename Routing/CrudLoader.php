<?php
/**
 * User: kay
 * Date: 01.10.14
 * Time: 21:08
 */

namespace Hn\DoctrineCrudBundle\Routing;


use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\Config\Loader\LoaderResolverInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class CrudLoader implements LoaderInterface
{
    const TYPE = 'hn_doctrine_crud';

    /** @var bool */
    protected $loaded = false;

    /** @var array */
    protected $config;

    /** @var \Symfony\Component\PropertyAccess\PropertyAccessor */
    protected $pa;

    /**
     * @param $config
     */
    public function __construct($config)
    {
        $this->config = $config;
        $this->pa = PropertyAccess::createPropertyAccessor();
    }

    public function load($resource, $type = null)
    {

        if (true === $this->loaded) {
            throw new \RuntimeException('Routing Loader "CrudLoader" must not be loaded twice!');
        }

        $routes = new RouteCollection();

        foreach ($this->config as $crudName => $crudConfig) {

            $controllerServiceId = "hn_doctrine_crud.$crudName.controller";

            if ($this->pa->getValue($crudConfig, '[controller][actions][new][disabled]') !== true) {
                // prepare a route for the "new" action
                $path = sprintf($this->pa->getValue($crudConfig, '[controller][actions][new][path]'), $crudName);
                $defaults = array('_controller' => "$controllerServiceId:newAction");
                $requirements = array();
                $methods = array('GET');

                $routeName = "$type.$crudName.new";
                $route = new Route($path, $defaults, $requirements, array(), '', array(), $methods);

                $routes->add($routeName, $route);

                // prepare a route for the "create" action
                $path = sprintf($this->pa->getValue($crudConfig, '[controller][actions][new][path]'), $crudName);
                $defaults = array('_controller' => "$controllerServiceId:createAction");
                $requirements = array();
                $methods = array('POST');

                $routeName = "$type.$crudName.create";
                $route = new Route($path, $defaults, $requirements, array(), '', array(), $methods);

                $routes->add($routeName, $route);
            }

            if ($this->pa->getValue($crudConfig, '[controller][actions][list][disabled]') !== true) {
                // prepare a route for the "list" action
                $path = sprintf($this->pa->getValue($crudConfig, '[controller][actions][list][path]'), $crudName);
                $defaults = array('_controller' => "$controllerServiceId:listAction");
                $requirements = array();
                $methods = array('GET');

                $routeName = "$type.$crudName.list";
                $route = new Route($path, $defaults, $requirements, array(), '', array(), $methods);

                $routes->add($routeName, $route);
            }

            if ($this->pa->getValue($crudConfig, '[controller][actions][edit][disabled]') !== true) {
                // prepare a route for the "edit" action
                $path = sprintf($this->pa->getValue($crudConfig, '[controller][actions][edit][path]'), $crudName);
                $defaults = array('_controller' => "$controllerServiceId:editAction");
                $requirements = array();
                $methods = array('GET');

                $routeName = "$type.$crudName.edit";
                $route = new Route($path, $defaults, $requirements, array(), '', array(), $methods);

                $routes->add($routeName, $route);

                // prepare a route for the "update" action
                $path = sprintf($this->pa->getValue($crudConfig, '[controller][actions][edit][path]'), $crudName);
                $defaults = array('_controller' => "$controllerServiceId:updateAction");
                $requirements = array('id' => '\d+');
                $methods = array('PUT');

                $routeName = "$type.$crudName.update";
                $route = new Route($path, $defaults, $requirements, array(), '', array(), $methods);

                $routes->add($routeName, $route);
            }

            if ($this->pa->getValue($crudConfig, '[controller][actions][delete][disabled]') !== true) {
                // prepare a route for the "delete" action
                $path = sprintf($this->pa->getValue($crudConfig, '[controller][actions][delete][path]'), $crudName);
                $defaults = array('_controller' => "$controllerServiceId:deleteAction");
                $requirements = array('id' => '\d+');
                $methods = array('DELETE');

                $routeName = "$type.$crudName.delete";
                $route = new Route($path, $defaults, $requirements, array(), '', array(), $methods);

                $routes->add($routeName, $route);
            }

        }

        $this->loaded = true;

        return $routes;
    }

    public function supports($resource, $type = null)
    {
        return self::TYPE === $type;
    }

    public function getResolver()
    {
    }

    public function setResolver(LoaderResolverInterface $resolver)
    {
    }
}